""" En Python las cadenas de texto se escriben entre comillas dobles o simples a continucion se muestra 
algunos ejemplos """  

'Hola bienvenido al mejor repositorio del mundo'

"El limite es el cielo"

""" Para poner comillas simples dentro de comillas simples o dobles estas deben de ser escapados colocando
una barra invertida delante de ellas, por ejemplo """

'La tienda Neon\'s venden luces'

""" Los saltos de linea se dan automaticos solamenta dando un enter cuando el texto se encuentra entre triples
comillas y cuando son comillas simples o dobles se pone \n, por ejemplo """

"""Fernando alonso es el
mejor piloto de la F1"""

'Fernando alonso es\n el mejor piloto de la F1'


"""Para generar una salida se escribe la palabra print en seguida de parentesis y dentro de ellos lo que se 
quiera mostrar al usuario, por ejemplo"""

print(3 * 2)
#Salida = 6

print('Hola mundo\n Desde Madrid')
"""Salida = Hola mundo
         Desde Madrid"""

"""Para generar una entrada del usuariose usa la funcion input, Por ejemplo"""

input('Seleccione un número del 0 al 10')

"""Entrada = numero que selecciono el usuario Ej. 8 """


"""Para realizar una concatenacion se pueden escribir con comillas simples o dobles, ya sea que contengan texto 
numeros, Por ejemplo"""

print("tel" + 'araña')
#Salida = telaraña

print("2" + "4")
#Salida = 24

"""Para la concatenacion de cadenas, se puede multiplicar por numeros enteros, haciendo repeticiones del texto o numeros,
Por ejemplo"""

print("Trek" * 2)
#Salida = TrekTrek

print('0' * 8)
#Salida = 00000000


"""Para la Conversion de tipos se realizan de la siguiente manera"""

int("2") + ("3")
#Salida = 5

"""Los Operadores tambien se pueden usar en cadenas, por ejemplo"""

a = 'Alienware'
print(x)
#Salida = Alienware
x += "Laptop"
print(x)
#Salida = AlienwareLaptop



""" En Python las sentencias normalmente se utilizan para ejecutar un código si cierta condicion se cumple """  

if 8 > 2:
	print('8 es mayor que 2')
print('Gracias por participar')
#Salida = 8 es mayor que 2
#         Gracias por participar

#La sentencia if puede ser anidada esto quiere decir que una sentencia if interna es la sentencia de la exterior

a = 15
if a > 12:
	print('a es mas grande que 12')
	if a <= 20:
		print('a esta entre 12 y 47')

#Salida = a es mas grande que 12
#         a esta entre 12 y 47


#La sentencia else se pone despues de una sentencia if, esto es por si se obtiene un false despues del if, y se pueden anidar 
#las veces que sean necesarias

t = 2
if t == 0:
	print('Es correcto mi chavo')
else:
	print('No es correcto mi chavo')

#La sentencia elif es una combinacion entre la sentencia if y la sentencia else

num = 8 
si num == 2: 
   print ("El número es 2") 
elif  num == 18: 
   print ("El número es 18") 
elif  num == 8: 
   print ("El número es 8") 
else:
    print (" El número no es 2, 18 o 8 ")
#Salida = El numero es 8
""" En Python las operaciones se realizan de manera muy practica a continucion se muestra 
algunos ejemplos """  

5 + 5
# Salida = 10

20 - 10
# Salida = 10

(5 + 10) * 5
# Salida = 250

20 / 5
# Salida = 4

(-8 + 2) * (-4)
# Salida = 24

3 / 4
# Salida = 0.75

5 ** 5
# Salida = 3125

#Division entera se escribe con //
20 // 6
# Salida = 3

# EL módulo se escribe con %
20 % 6
# Salida = 2

#Los operadores son +,-,*,/ y %. Se pueden utilizar con numeros, por ejemplo
p = 8
print(p)
#Salida = 8
p += 10
print(p)
#Salida = 18
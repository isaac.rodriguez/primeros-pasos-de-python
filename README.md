# Primeros pasos de Python 3

Dentro de este repositorio, se encuentra un pequeño tutorial, para aprender las 
bases principales del lenguaje de programacion python 3. 

# ¿Que es Python?
El nombre Python proviene del surrealista grupo de comedia británica Monty 
Pitón,no de la serpiente. A los programadores de Python se les llama 
cariñosamente Pythonistas, y tanto las referencias a Monty Python como a las 
serpientes suelen ser de pimienta.
Python es un lenguaje de programación de alto nivel, con aplicaciones en 
numerosas áreas, que incluyen programación web, scripts, computación científica 
e inteligencia artificial. 
Python es procesado en tiempo de ejecución por el intérprete . No es necesario 
compilar su programa antes de ejecutarlo.
Es muy popular y utilizado por organizaciones como Google, la NASA, la CIA y 
Disney.

# Instalación 
El intérprete de Python se puede descargar gratuitamente desde http://python.org, 
y hay versiones para Linux, OS X y Windows.
Tenga en cuenta la >>> en el código de arriba. Son el símbolo de la consola de 
Python. Python es un lenguaje interpretado, lo que significa que cada línea se 
ejecuta a medida que se ingresa. Python también incluye IDLE , el entorno de 
desarrollo integrado, que incluye herramientas para escribir y depurar programas
completos.
Encontrará instaladores de Python para equipos de 64 y 32 bits para cada sistema
operativo en la página de descargas, así que primero averigüe qué instalador 
necesita. Si compró su computadora en 2007 o después, lo más probable es que sea
un sistema de 64 bits. De lo contrario, tiene una versión de 32 bits, pero aquí 
está cómo averiguarlo con seguridad:

**Windows**
* Seleccione Start4Control Panel4System y compruebe si System Type 
dice 64 bits o 32 bits.
* Despues descargue el instalador de Python (el nombre de archivo 
terminará con.msi) y haga doble clic en él. Siga las instrucciones del 
instalador en la pantalla para instalar Python, como se indica aquí:
    1. Seleccione Instalar para todos los usuarios y, a continuación, haga 
    clic en Siguiente.
    2. Instale en la carpeta C:\Python34 haciendo clic en Siguiente.
    3. Haga clic de nuevo en Siguiente para omitir la sección Personalizar 
    Python.

**IDLE:** Si usted tien Windows XP, haga click en el botón Inicio y seleccione 
Programas y luego IDLE. Si tiene Windows 7 o posterior haga click en el icono
de inicio e introduza IDLE.

**OS X**
* Vaya al menú de Apple, seleccione Acerca de este Mac4Más 
información4System Report4Hardware, y luego mire el nombre del procesador campo.
Si dice Intel Core Solo o Intel Core Duo, usted tiene una maquina con disco duro
de 32 bits. Si dice algo más (incluyendo Intel Core 2 Duo), usted tienen una 
máquina de 64 bits.
* Despues descargue el archivo.dmg adecuado para su versión de OS X y haga 
doble clic en él. Siga las instrucciones que el instalador muestra en la pantalla
para instalar Python, como se indica aquí:
    1. Cuando el paquete DMG se abra en una nueva ventana, haga doble clic en el
    botón Archivo Python.mpkg. Es posible que tenga que introducir la contraseña
    de administrador.
    2. Haga clic en Continuar en la sección de Bienvenida y haga clic en Aceptar
    para aceptar. la licencia.
    3. Seleccione HD Macintosh (o el nombre que tenga su disco duro) y haga clic
    en Instalar.

**IDLE:** Abra la ventana Finder, haga clic en Aplicaciones, haga clic en Python 
3.4 y, a continuación, en el icono IDLE. 


**Ubuntu Linux**
* Abra un Terminal y ejecute el comando uname -m. Una respuesta
de i686 significa 32 bits, y x86_64 significa 64 bits.
* Despues instale Python siguiendo estos pasos:
    1. Introduzca sudo apt-get install python3.
    2. Introduzca sudo apt-get install idle3.
    3. Introduzca sudo apt-get install python3-pip.
    
**IDLE:** Seleccione Applications4Accessories4Terminal y luego ingrese idle3. 
(También puede hacer clic en Applications en la parte superior de la pantalla, 
seleccionar Programming, y luego en IDLE.). 

# Contenido
**Conceptos Básicos**
  * Hello World.
  * Operaciones básicas.
  * Cadenas.
  * Variables.
  * IDLE.
    
**Estructuras de Control**
  * Booleanos.
  * Sentencias. 
 
# Autor
**HÉCTOR OCTAVIO GIRÓN BOBADILLA**





""" En Python los booleanos se componen de dos valores, Verdadero o falso (True o False), se pueden crear comparando con el operador
igual igual (==)  """  

mi_var = True
mi_var
#Salida = True

8 == 10
#Salida = False

a == a
#Salida = True

#El operador de desigualdad (!=) en el cual da como resultado True si lo que se compara no es igual y False si lo son

1 != 1
#Salida = False

1 != 8
#Salida = True

#El operador de comparacion mayor, menor, o igual (<,>)
 
 8 > 1
 #Salida = True

10 <= 9
#Salida = True



